import React, { useState, useEffect } from "react";
import "./App.css";
// import ResultComponent from "./components/ResultComponent";
// import KeyPadComponent from "./components/KeyPadComponent";
import axios from "axios";
import qs from "qs";
function App() {
  const [list, setList] = useState([]);

  useEffect(() => {
    var data = qs.stringify({
      data: "1",
    });
    var config = {
      method: "get",
      url: "https://restcountries.com/v3.1/all",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(response.data[0]);

        setList(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);
 

  return (
    <div>
      <div className="calculator-body">
        <h1>Simple Calculator</h1>
        {list.map((i, index) => (
          <div key={index}>
            <span>{i.region}</span>
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
